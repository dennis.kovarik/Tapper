//
//  ViewController.swift
//  Tapper
//
//  Created by Virginia on 11/30/19.
//  Copyright © 2019 South Dakota School of Mines. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var timer: UILabel!
    @IBOutlet weak var ButtonLabel: UIButton!
    @IBOutlet weak var tapCount: UILabel!
    
    var tapperActive = false
    var getReady = false
    var coolDown = false
    var time = 0
    var taps = 0
    
    // Timer
    var appleTimer = Timer()
    
    @IBAction func tapButton(_ sender: Any)
    {
        // Countdown timer to allow user to get ready to tap
        if(tapperActive == false && time <= 0 && getReady == false && coolDown == false)
        {
            getReady = true
            time = 3
            taps = 0
            timer.text = String(time)
            ButtonLabel.setTitle("Get Ready", for: .normal)
            appleTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.action), userInfo: nil, repeats: true)
            taps = 0
            tapCount.text = String(taps)
        }
        
        if(tapperActive)
        {
            taps += 1
            tapCount.text = String(taps)
        }
    }
    
    @objc func action()
    {
        // Countdown timer to allow user to get ready to tap
        if(tapperActive == false && time <= 0 && coolDown == false)
        {
            // Set time limit for user to tap during
            time = 11
            tapperActive = true
            ButtonLabel.setTitle("Tap!", for: .normal)
        }
        else if(time == 0 && tapperActive == true && coolDown == false)
        {
            ButtonLabel.setTitle("Start", for: .normal)
            tapperActive = false
            getReady = false
            coolDown = true
            return
        }
        
        if(time <= -3)
        {
            time = 0
            getReady = false
            appleTimer.invalidate()
            coolDown = false
            tapperActive = false
            return
        }
        
        time -= 1
        
        if(coolDown == false)
        {
            timer.text = String(time)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

